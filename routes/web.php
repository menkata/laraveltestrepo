<?php

Route::get('/', 'TaskController@index');

Route::get('/tasks', 'TaskController@tasks');

Route::post('/task', 'TaskController@store');

Route::get('/tasks/{id}', 'TaskController@getTask');

Route::post('/tasks/{id}/comments', 'CommentsController@store');

Route::get('/lay', function(){
    return view('layout');
});

Route::get('/test', function(){
    return view('myTestViewForRout');
});

/* Route::get('/testRout', function () {
    $list = DB::table('tasks')->get();
    return view('tasks.index', compact('list'));
}); */



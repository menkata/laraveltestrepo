<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;

class Task extends Model
{

    public function comments()
    {
        return $this->hasMany(Comment::class);//return class path like App\Comment;
    }

    public function addComment($body)
    {
        Comment::create([
            'body' => request('body'),
            'task_id' => $this->id
        ]);
    }

/*     public function scopeCompleted($query)
    {
        return $query->where('completed',1);
    }

    public static function scopeCheckId($query)
    {
        return $query->where('id','<',3);
    }

    protected $fillable = [
        'title',
        'body'
    ];

    protected $guarded = [
        'id'
    ]; */
}

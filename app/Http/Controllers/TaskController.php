<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;

class TaskController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function tasks(Request $request)
    {
        echo $request->is('tasks');
        $list = Task::get();
        return view('tasks.index', compact('list'));    
    }

    public function store(Request $request)
    {
/*         $task = new Task;

        $task->body = request('body');

        $task->completed = request('completed');
        
        $task->save(); */

        $this->validate(request(),[
            'title' => 'required|min:3',
            'body' => 'required|min:3'
        ]);

        Task::create(request(['title','body']));

        return redirect('/');
    }

    public function getTask(Task $id) //Task::find(wildcard) {wildcard must be primary key}
    {
        //$list = Task::find($id);
        return view('tasks.show', compact('id'));
    }


}
